package com.example.android.finals;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.beardedhen.androidbootstrap.BootstrapButton;
import com.beardedhen.androidbootstrap.BootstrapEditText;

import org.json.JSONObject;

import java.util.HashMap;


public class Try_us extends AppCompatActivity implements View.OnClickListener {

    Toolbar toolbar;
    com.beardedhen.androidbootstrap.BootstrapEditText name, phone_no, nego, email;
    com.beardedhen.androidbootstrap.BootstrapButton submit;
    Progress progress = new Progress(this);
    Context context;
    RequestQueue queue;
    ConnectionDetector connectionDetector = new ConnectionDetector(this);
    ErrorDialogMessage errorDialogMessage;
    android.support.design.widget.TextInputLayout Errorname, Errorphone, Errornego, Erroremail;

    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_try_us);
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        name = (BootstrapEditText) findViewById(R.id.etName);
        phone_no = (BootstrapEditText) findViewById(R.id.etNumber);
        nego = (BootstrapEditText) findViewById(R.id.etNegotiated);
        email = (BootstrapEditText) findViewById(R.id.etEmail);
        submit = (BootstrapButton) findViewById(R.id.submit);
        Errorname = (TextInputLayout) name.getParent();
        Errorphone = (TextInputLayout) phone_no.getParent();
        Errornego = (TextInputLayout) nego.getParent();
        Erroremail = (TextInputLayout) email.getParent();
        queue = VolleySingleton.getInstance().getRequestQueue();
        context = this;
        submit.setOnClickListener(this);
        errorDialogMessage = new ErrorDialogMessage(this);
    }

    private void generateError(TextInputLayout t, String Message) {

        t.setErrorEnabled(true);
        t.setError(Message);
    }

    private void parseData(String email, String name, String number, String nego) {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("name", name);
        params.put("phone", number);
        params.put("email", email);
        params.put("negotiate", nego);
        String url = "http://dexesnotes.net84.net/register_nego.php";

        CustomRequest jsObjRequest = new CustomRequest(Request.Method.POST, url, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    String data = response.getString("inserted");
                    if (data.equals("1")) {
                        errorDialogMessage.show("Your response has been submitted we shall contact you shortly.");

                    } else {
                        Toast.makeText(context, "Try again!!", Toast.LENGTH_LONG).show();
                    }
                    progress.stop();
                } catch (Exception e) {
                    Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progress.stop();
            }
        });
        jsObjRequest.setTag("SetupAccount");
        queue.add(jsObjRequest);
    }


    @Override
    public void onClick(View v) {
        int count = 0;
        if (connectionDetector.isConnectingToInternet()) {
            String names = name.getText().toString();
            String phone_nos = phone_no.getText().toString();
            String negos = nego.getText().toString();
            String emails = email.getText().toString();
            if (!isValidEmail(emails)) {
                generateError(Erroremail, "    *Enter Valid Email");
            } else {
                count++;
                Erroremail.setErrorEnabled(false);
            }
            if (phone_nos.length() != 10) {
                generateError(Errorphone, "    *Enter Valid Phone No.");
            } else {
                count++;
                Errorphone.setErrorEnabled(false);
            }
            if (names.length() == 0) {
                generateError(Errorname, "    *Enter Name");
            } else {
                count++;
                Errorname.setErrorEnabled(false);
            }
            if (negos.length() == 0) {
                generateError(Errornego, "    *Enter Some Negotiation");
            } else {
                count++;
                Errornego.setErrorEnabled(false);
            }

            if (count == 4) {
                progress.show();
                parseData(emails, names, phone_nos, negos);
            } else {
                Toast.makeText(context, "FILL OUT MISSING DETAILS", Toast.LENGTH_SHORT).show();
            }
        } else {
            errorDialogMessage.show();
        }
    }
}


