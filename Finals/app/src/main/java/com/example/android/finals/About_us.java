package com.example.android.finals;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.ScrollView;

import com.beardedhen.androidbootstrap.AwesomeTextView;

public class About_us extends AppCompatActivity implements View.OnClickListener {
    Toolbar toolbar;
    ScrollView scrollView;
    com.beardedhen.androidbootstrap.AwesomeTextView developed;
    ImageView fb, gplus, insta, tweet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        scrollView = (ScrollView) findViewById(R.id.scrollView);
        scrollView.setFadingEdgeLength(225);
        developed = (AwesomeTextView) findViewById(R.id.developed);
        fb = (ImageView) findViewById(R.id.fb);
        gplus = (ImageView) findViewById(R.id.gplus);
        insta = (ImageView) findViewById(R.id.insta);
        tweet = (ImageView) findViewById(R.id.tweet);
        fb.setOnClickListener(this);
        gplus.setOnClickListener(this);
        insta.setOnClickListener(this);
        tweet.setOnClickListener(this);
        developed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent, chooser;
                intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("https://www.facebook.com/abhishek.thakur.180"));
                chooser = Intent.createChooser(intent, "Complete action using");
                startActivity(chooser);

            }
        });
    }

    @Override
    public void onClick(View v) {
        Intent intent, chooser;
        switch (v.getId()) {
            case R.id.fb:
                intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("https://www.facebook.com/negokids"));
                chooser = Intent.createChooser(intent, "Complete action using");
                startActivity(chooser);
                break;
            case R.id.gplus:
                intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("https://plus.google.com/app/basic/108523798326869175027"));
                chooser = Intent.createChooser(intent, "Complete action using");
                startActivity(chooser);
                break;
            case R.id.tweet:
                intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("https://twitter.com/negokidsindia"));
                chooser = Intent.createChooser(intent, "Complete action using");
                startActivity(chooser);
                break;
            case R.id.insta:
                intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("https://www.instagram.com/negokids/"));
                chooser = Intent.createChooser(intent, "Complete action using");
                startActivity(chooser);
                break;
        }
    }
}
