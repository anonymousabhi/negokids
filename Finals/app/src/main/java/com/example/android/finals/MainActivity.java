package com.example.android.finals;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    Toolbar toolbar;
    ImageView tryus, pricing, vedios, aboutus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        tryus = (ImageView) findViewById(R.id.try_us);
        pricing = (ImageView) findViewById(R.id.pricing);
        vedios = (ImageView) findViewById(R.id.vedios);
        aboutus = (ImageView) findViewById(R.id.about_us);
        tryus.setOnClickListener(this);
        pricing.setOnClickListener(this);
        vedios.setOnClickListener(this);
        aboutus.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.try_us:
                startActivity(new Intent(this, Try_us.class));
                break;
            case R.id.pricing:
                startActivity(new Intent(this, Pricing.class));
                break;
            case R.id.vedios:
                startActivity(new Intent(this, Vedios.class));
                break;
            case R.id.about_us:
                startActivity(new Intent(this, About_us.class));
                break;
        }
    }
}
