package com.example.android.finals;

/**
 * Created by abhishek on 1/11/2016.
 */


import android.app.Application;
import android.content.Context;

public class MyApplication extends Application {


    public static MyApplication sInstance;

    public static MyApplication getInstance() {

        return sInstance;
    }

    public static Context getAppContext() {

        return sInstance.getApplicationContext();
    }

    @Override
    public void onCreate() {

        super.onCreate();
        sInstance = this;
    }


}